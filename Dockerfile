FROM docker
RUN /sbin/apk update --no-cache \
    && /sbin/apk add git --no-cache \
    && /bin/rm -rf /var/cache/apk/*
COPY --from=docker/buildx-bin /buildx /usr/libexec/docker/cli-plugins/docker-buildx
RUN docker buildx version
